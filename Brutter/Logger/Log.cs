﻿using System;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Brutter.Logger
{
    internal class Log
    {
        private TextBox loggerArea;
        private Dispatcher dispatcher;

        public Log(TextBox loggerArea,Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            this.loggerArea = loggerArea;
        }

        public void Info(string message)
        {
            dispatcher.BeginInvoke(new Action(()=>
            loggerArea.Text = loggerArea.Text + GetDateTime() + ":INFO - " + message + "\n"));
        }

        public void Error(string message)
        {
            dispatcher.BeginInvoke(new Action(() =>
            loggerArea.Text = loggerArea.Text + GetDateTime() + ":ERROR - " + message + "\n"));
        }

        public void Warning(string message)
        {
            dispatcher.BeginInvoke(new Action(() =>
            loggerArea.Text = loggerArea.Text + GetDateTime() + ":WARNING - " + message + "\n"));
        }

        private string GetDateTime()
        {
            string retValue = string.Empty;
            DateTime dt = DateTime.Now;
            return retValue = dt.ToShortDateString() + " " + dt.ToLongTimeString();
        }
    }
}