﻿using System;

namespace Brutter
{
    public class BrutterExceptions : Exception
    {
        public BrutterExceptions(string message) : base(message)
        {
        }
    }

    public class ErrorLoadingProxyList : BrutterExceptions
    {
        public ErrorLoadingProxyList(string message) : base(message)
        {
        }
    }
}