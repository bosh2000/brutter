﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brutter.Datas
{
    public class Request
    {
        public string id { get; set; }
        public string password { get; set; }
        public string referer { get; set; }
        public bool remember { get; set; }
    }
}
