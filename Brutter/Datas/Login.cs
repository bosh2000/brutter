﻿using Newtonsoft.Json;

namespace Brutter.Datas
{
    internal class Login
    {
        public string _login;
        public string Id {
            get { return _login+ "@optimum.net"; }
            set {
                string[] strArray = new string[2];
                strArray = value.Split('@');
                _login = strArray[0];
            } }
        public string Password;

        public Login(string LoginPasswordString)
        {
            string[] strArray = new string[2];
            strArray = LoginPasswordString.Split(':');
            this.Password = strArray[1];
            this.Id = strArray[0];
        }

        public string GetJsonRequestBody()
        {
            var retValue = string.Empty;
            Request rqs = new Request();
            rqs.id = this.Id;
            rqs.password = Password;
            rqs.remember = false;
            rqs.referer = "https://www.optimum.net/";
            retValue = JsonConvert.SerializeObject(rqs);
            return retValue;
        }
    }
}