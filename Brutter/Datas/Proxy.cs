﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brutter.Datas
{
    class Proxy
    {
        public string Ip { get; set; }
        public string Port { get; set; }

        public Proxy(string proxyString)
        {
            string[] strValue = new string[2];
            strValue = proxyString.Split(':');
            this.Ip = strValue[0];
            this.Port = strValue[1];
        }
    }
}
