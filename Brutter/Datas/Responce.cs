﻿namespace Brutter.Datas
{
    public class Responce
    {
        public object soptid { get; set; }
        public bool softPaved { get; set; }
        public string status { get; set; }
        public bool session_created { get; set; }
        public object userProfile { get; set; }
        public string redirect_to { get; set; }
        public object xsrf_token { get; set; }
        public BanInfo ban_info { get; set; }

        public class BanInfo
        {
            public bool ban_in_effect { get; set; }
            public int ban_expiration_time { get; set; }
            public string banned_token { get; set; }
            public int attempts_remaining { get; set; }
            public int failed_attempts { get; set; }
            public int ban_time_left { get; set; }
        }
    }
}