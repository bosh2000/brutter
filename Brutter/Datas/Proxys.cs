﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Brutter.Datas
{
    internal class Proxys : IEnumerable<Proxy>
    {
        private List<Proxy> proxysList;
        private string proxyListFileName;
        private int indexList;
        private int indexUsedProxy;

        public Proxys(string proxyListFileName)
        {
            this.proxysList = new List<Proxy>();
            this.proxyListFileName = proxyListFileName;
            this.indexList = 0;
            this.indexUsedProxy = 0;
        }

        public IEnumerator<Proxy> GetEnumerator()
        {
            return proxysList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return proxysList.GetEnumerator();
        }

        public void Add(string proxyString)
        {
            proxysList.Add(new Proxy(proxyString));
        }

        public void LoadProxyListFromFile()
        {
            LoadProxyListFromStream(new StreamReader(this.proxyListFileName));
            //using (StreamReader fread = new StreamReader(this.proxyListFileName))
            //{
            //    while (!fread.EndOfStream)
            //    {
            //        var str = fread.ReadLine();
            //        Add(str);
            //    }
            //}
        }

        public void LoadProxyListFromStream(StreamReader stream)
        {
            while (!stream.EndOfStream)
            {
                var str = stream.ReadLine();
                Add(str);
            }
        }

        public string GetNextProxy()
        {
            string strValue = string.Empty;

            if (indexList == proxysList.Count) { indexList = 0; }
            Proxy proxy = proxysList[indexList];
            strValue = $"{proxy.Ip}:{proxy.Port}";
            if (indexUsedProxy >= 10)
            {
                indexList++;
                indexUsedProxy = 0;
            }
            indexUsedProxy++;
            return strValue;
        }
    }
}