﻿using System.Collections.Generic;

namespace Brutter.Datas
{
    public class UserData
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string zipcode { get; set; }
        public string accountNumber { get; set; }
        public bool optedInOptimumEmail { get; set; }
        public string optimumEmailDomain { get; set; }
        public string primaryId { get; set; }
        public string id { get; set; }
        public List<AlternateContact> alternate_contacts { get; set; }
        public Links _links { get; set; }
    }

    public class AlternateContact
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class Self
    {
        public string href { get; set; }
    }

    public class Links
    {
        public Self self { get; set; }
    }
}