﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brutter.Datas
{
    class Logins
    {
        public List<Login> ListLogins;

        public Logins(string FullFileName)
        {
            ListLogins = new List<Login>();
            FillLoginList(FullFileName);
        }

        public void Add(string LoginPasswordString)
        {
            ListLogins.Add(new Login(LoginPasswordString));
        }

        private void FillLoginList(string FullFileName)
        {
            using (StreamReader fileRead=new StreamReader(FullFileName))
            {
                string readString = string.Empty;
                while (!fileRead.EndOfStream )
                {
                    Add(fileRead.ReadLine());
                }
            }
        }


    }
}
